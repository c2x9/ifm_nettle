# ifm_nettle

#### introduction

The software is the interface adaptation layer of nettle, which supports  calling some encryption and decryption algorithms to the acceleration  capability provided by Kunpeng hardware, thus providing higher  encryption and decryption efficiency in Kunpeng scenarios. 

Please click on the link to introduce Kunpeng Hardware Acceleration Library

nettle official website：[http://www.lysator.liu.se/~nisse/nettle/](https://gitee.com/link?target=http%3A%2F%2Fwww.lysator.liu.se%2F~nisse%2Fnettle%2F)

#### software architecture

The interface is compatible with the nettle. It supports calling the  interface provided by the Kunpeng acceleration library for the  implementation of some encryption and decryption algorithms through  configuration. For the interface not implemented by hardware, continue  to call the interface of the original software for implementation. The  overall logical structure is as follows: 

```
                +----------------+
                |                |
                |   inf_nettle   |
                |                |
                +----------------+
                       /\
                      /  \
                     /    \
    +----------------+    +----------------+
    |                |    |                |
    |     nettle     |    |     KAE WD     |
    |                |    |    interface   |
    +----------------+    +----------------+
```

#### installation tutorial

1. Run the following command to install gmock-devel cmake 

```
yum install -y gmock-devel cmake make gcc-c++ nettle-devel libgcrypt-devel
```

2. Use the following command clone code 

```
git clone https://gitee.com/openeuler/ifm_nettle.git
```

3. Enter the ifm_nettle directory and execute the following command to compile 

```
sudo mkdir build
cd build
cmake ..
make
```

4. Execute the following command to run the test case: 

```
ctest
```

5. Execute the following command to install: 

```
make install
```

6. To perform a pressure test, execute the following command 

```
cd bench
./nettle-bench


        Algorithm         mode     Mbyte(512)/s      Mbyte(1K)/s     Mbyte(10K)/s    Mbyte(512K)/s      Mbyte(1M)/s     Mbyte(10M)/s     Mbyte(20M)/s
        ifm_sha224       update         2105.03          2070.77          2121.39          2067.91          2067.10          2084.74          2064.83
        ifm_sha256       update         2038.78          2093.81          2093.56          2092.40          2109.48          2098.20          2080.10
        ifm_sha384       update          591.92           619.40           561.98           652.62           587.42           633.66           618.05
        ifm_sha512       update          659.03           585.36           584.84           612.06           704.24           706.67           629.51
    ifm_sha512_224       update          644.40           640.75           582.09           630.41           608.11           676.31           631.03
    ifm_sha512_256       update          704.07           694.24           699.61           671.89           699.06           584.31           696.86
```

#### instructions for use

1. xxxx
2. xxxx
3. xxxx

#### Committer

@huangduirong [huangduirong@huawei.com](mailto:huangduirong@huawei.com)
